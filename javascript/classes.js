// the GAME (main)
class Game extends PIXI.Application {
    constructor() {
        super()

        // fundamentals
        this.sheets = {}
    }

    // create ground level
    /**
     * 
     * @param {Number} groundLevel 
     */
    setGround(groundLevel) {
        this.ground = groundLevel
    }

    // create and add canvas to document
    /**
     * 
     * @param {String} qs 
     */
    createCanvas(qs) {
        document.querySelector(qs).appendChild(this.view)
    }

    // load a single resource
    /**
     * 
     * @param {String} name 
     * @param {String} path 
     */
    addResource(name, path) {
        this.loader.add(name, path)
    }

    // create a sheet
    /**
     * @param {String} animationName
     * @param {String} resourceName
     * @param {Array<Number>} frameNumbers
     */
    createAnimation(animationName, resourceName, frameNumbers) {
        // create the sprite sheet
        var sheet = new PIXI.BaseTexture(this.loader.resources[resourceName].url)
        var w = this.loader.resources[resourceName].width
        var h = this.loader.resources[resourceName].height

        // create the obj
        if (!this.sheets[resourceName]) this.sheets[resourceName] = {}

        // create the animation
        this.sheets[resourceName][animationName] = []
        frameNumbers.forEach(fn => {
            var texture = new PIXI.Texture(sheet, new PIXI.Rectangle(fn * w, 0, w, h))
            this.sheets[resourceName][animationName].push(texture)
        })
    }

    // create player obj
    createPlayer() {
        // create player
        this.player = new Player(200, this.ground, "Jordan")

        // create animations' sprite sheets
        this.createAnimation('idle', 'player', [1, 2, 3])
        this.createAnimation('run', 'player', [8, 9, 10, 11])
        this.createAnimation('fly', 'player', [21, 22, 23, 21])
        this.createAnimation('jump', 'player', [28, 21, 22, 23, 21])
        this.createAnimation('land', 'player', [28, 28, 28])

        // configure player animation
        this.player.createAnimation(this.sheets.player.run)
    }

    // conf abilities
    configureAbilities() {
        // configure fire
        this.createAnimation('idle', 'fireball', [0, 1, 2, 3, 4])

        // configure the rest
        // TODO
    }
}

class Player {
    /**
     * @param {Number} x
     * @param {Number} y
     * @param {String} name
     */
    constructor(x, y, name) {
        this.name = name
        this.health = 100
        this.invulnerable = false
        this.cooldown = false
        this.flightLimit = 400
        this.projectiles = []
        this.x = x
        this.y = y
    }

    createAnimation(animationSheet) {
        if (this.animation) return

        // create animation
        this.animation = new PIXI.AnimatedSprite(animationSheet)
        this.animation.animationSpeed = 0.2
        this.animation.anchor.set(0.4, 1)
        this.animation.loop = false
        this.animation.x = this.x
        this.animation.y = this.y
        
        // create hitbox
        this.hitbox = new Hitbox(this.x, this.y, 40, 56, this.animation.anchor)
    }

    createFireball() {
        var fireball = new Projectile(x, y)
    }

    play() {
        // play animation
    }
}

class Hitbox {
    /**
     * 
     * @param {Number} x 
     * @param {Number} y 
     * @param {Number} w width
     * @param {Number} h height
     * @param {PIXI.Point} a anchor
     */
    constructor(x, y, w, h, a) {
        this.x0 = x - (w * a.x)
        this.x1 = x + this.x0
        this.y0 = y - (h * a.y)
        this.y1 = y + this.y0
        this.width = w
        this.height = h
        this.anchor = a
    }

    /**
     * 
     * @param {Number} x
     * @param {Number} y 
     */
    update(x, y) {
        this.x0 = x - (this.width * this.anchor.x)
        this.x1 = x + this.x0
        this.y0 = y - (this.height * this.anchor.y)
        this.y1 = y + this.y0
    }
}

class Projectile {
    /**
     * 
     * @param {Number} x 
     * @param {Number} y 
     */
    constructor(x, y) {
        // this.animation = 
    }
}