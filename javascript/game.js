var game

// initialize game
function init() {
    // init game object
    game = new Game({
        width: window.innerWidth / 1.1,
        height: window.innerHeight / 1.1,
        transparent: false,
        resolution: 1
    });

    // create ground
    game.setGround(game.view.height - (game.view.height / 12))

    // create canvas
    game.createCanvas('body')

    // add resources
    game.addResource('ground', 'resources/ground.png')
    game.addResource('background', 'resources/bg.png')
    game.addResource('player', 'resources/character.png')
    game.addResource('fireball', 'resources/fire_bullet.png')

    // create player
    game.createPlayer()

    // configure abilities
    game.configureAbilities()
}